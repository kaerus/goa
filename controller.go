package goa

import (
	"fmt"

	"net/http"
	"os"
	"reflect"
	"runtime"

	"bitbucket.org/kaerus/goa/stick"
	"bitbucket.org/kaerus/goa/stick/twig"
)

type Handler func(http.ResponseWriter, *http.Request)

type ProxyHandler func(http.ResponseWriter, *http.Request, func(*http.Request))

type RequestMethod struct {
	Type   string
	Path   string
	Handle Handler
}

var Controllers map[string]*Controller

type Controller struct {
	Name    string
	Mount   string
	methods []RequestMethod
	proxy   *Proxy
	stick   *stick.Env
}

type Proxy struct {
	handle ProxyHandler
	next   *Proxy
}

var stickEnv *stick.Env

func init() {
	Controllers = make(map[string]*Controller)
}

func AddController(c func(*Controller)) *Controller {
	var (
		name  string
		mount string
	)

	name = runtime.FuncForPC(reflect.ValueOf(c).Pointer()).Name()

	if stickEnv == nil {
		docDir := Config.Get("doc_dir").(string)
		stickEnv = twig.New(stick.NewFilesystemLoader(docDir))
	}

	CustomFilters(stickEnv)

	controller := &Controller{Name: name, Mount: mount, stick: stickEnv}
	c(controller)
	Controllers[controller.Name] = controller
	logger.Info(controller.Name)
	return controller
}

// Get ...
func (c *Controller) Get(path string, handler Handler) *Controller {
	return c.Register(http.MethodGet, path, handler)
}

// Head ...
func (c *Controller) Head(path string, handler Handler) *Controller {
	return c.Register(http.MethodHead, path, handler)
}

// Put ...
func (c *Controller) Put(path string, handler Handler) *Controller {
	return c.Register(http.MethodPut, path, handler)
}

// Patch ...
func (c *Controller) Patch(path string, handler Handler) *Controller {
	return c.Register(http.MethodPatch, path, handler)
}

// Post ...
func (c *Controller) Post(path string, handler Handler) *Controller {
	return c.Register(http.MethodPost, path, handler)
}

// Delete ...
func (c *Controller) Delete(path string, handler Handler) *Controller {
	return c.Register(http.MethodDelete, path, handler)
}

// Connect ...
func (c *Controller) Connect(path string, handler Handler) *Controller {
	return c.Register(http.MethodConnect, path, handler)
}

// Options ...
func (c *Controller) Options(path string, handler Handler) *Controller {
	return c.Register(http.MethodOptions, path, handler)
}

// Trace ...
func (c *Controller) Trace(path string, handler Handler) *Controller {
	return c.Register(http.MethodTrace, path, handler)
}

// Register ...
func (c *Controller) Register(method string, path string, handler Handler) *Controller {
	c.methods = append(c.methods, RequestMethod{method, path, handler})

	return c
}

func (p *Proxy) handler(w http.ResponseWriter, req *http.Request, fn Handler) {
	proxy := p.next

	p.handle(w, req, func(request *http.Request) {
		if request == nil {
			request = req
		}
		if proxy != nil {
			proxy.handler(w, request, fn)
		} else {
			fn(w, request)
		}
	})
}

func (c *Controller) Methods() []RequestMethod {
	methods := c.methods

	if c.proxy != nil {
		for i := range methods {
			method := &methods[i]
			handler := method.Handle
			method.Handle = func(w http.ResponseWriter, req *http.Request) {
				c.proxy.handler(w, req, handler)
			}
		}
	}

	return methods
}

// Use proxy handler
func (c *Controller) Use(p ProxyHandler) *Controller {
	proxy := &Proxy{p, nil}

	if c.proxy == nil {
		c.proxy = proxy
	} else {
		node := c.proxy
		for ; node.next != nil; node = node.next {
		}
		node.next = proxy
	}

	return c
}

func (c *Controller) Render(w http.ResponseWriter, req *http.Request, template string, values map[string]stick.Value) {
	var lang string

	if cookie, err := req.Cookie("lang"); err == nil {
		lang = cookie.Value
	} else {
		lang = "en"
	}

	values["lang"] = lang

	file := "templates/" + template

	if err := c.stick.Execute(file, w, values); err != nil {
		errMsg := fmt.Errorf("%s: %v", file, err)
		errCode := 0
		if os.IsNotExist(err) {
			errCode = http.StatusNotFound
		} else if os.IsPermission(err) {
			errCode = http.StatusForbidden
		} else {
			errCode = http.StatusUnprocessableEntity
		}

		http.Error(w, errMsg.Error(), errCode)

	}
}
