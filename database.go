package goa

import (
	"context"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
)

var driver string
var globalDB *sqlx.DB

func (s *Server) Connect(dbDriver, dsn string, options ...map[string]interface{}) (*sqlx.DB, error) {
	// MyColumnID -> my_column_id
	sqlx.NameMapper = decapitalizer

	driver = dbDriver
	db, err := sqlx.Connect(driver, dsn)

	globalDB = db
	Config.Set("db", db)

	if err != nil {
		return nil, err
	}

	if len(options) > 0 {
		for opt, val := range options[0] {
			switch opt {
			case "conn_max_lifetime":
				db.SetConnMaxLifetime(val.(time.Duration))
			case "max_idle_conns":
				db.SetMaxIdleConns(val.(int))
			case "max_open_conns":
				db.SetMaxOpenConns(val.(int))
			default:
				db.Query("SET "+opt+" = ?", val)
			}
		}
	}

	// check connection
	if err = db.Ping(); err != nil {
		return db, err
	}

	return db, nil
}

func decapitalizer(str string) string {
	result := ""

	uc := false

	for i, s := range str {
		if 'A' <= s && s <= 'Z' {
			s += 'a' - 'A'
			if i > 0 && !uc {
				result += "_"
			}
			uc = true
		} else if uc {
			if i > 1 && result[len(result)-2:len(result)-1] != "_" {
				result += "_"
			}
			uc = false
		}

		result += string(s)
	}

	return result
}

func Database(db interface{}) func(http.ResponseWriter, *http.Request, func(*http.Request)) {
	return func(w http.ResponseWriter, req *http.Request, next func(*http.Request)) {
		request := req.WithContext(context.WithValue(req.Context(), DatabaseKey, db))
		next(request)
	}
}

func DB(req *http.Request) *sqlx.DB {
	return req.Context().Value(DatabaseKey).(*sqlx.DB)
}

type Prepared struct {
	Query     string
	Statement *sqlx.NamedStmt
}

// Statement handles prepared statements
// Statement("<Prepared>") returns a prepared statement
// Returns *sqlx.Stmt
func Statement(p *Prepared) *sqlx.NamedStmt {

	// let it panic if p is nil
	if p.Statement == nil {
		stmt, err := globalDB.PrepareNamed(p.Query)
		if err != nil {
			logger.Error(err)
			return nil
		}
		p.Statement = stmt
	}

	return p.Statement
}
