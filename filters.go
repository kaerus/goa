package goa

import (
	"fmt"

	"bitbucket.org/kaerus/goa/stick"
	"github.com/vube/i18n"
)

type stickFilter = func(ctx stick.Context, val stick.Value, args ...stick.Value) stick.Value

var customFilters = map[string]stickFilter{
	"trans":   trans,
	"plural":  plural,
	"numeral": numeral,
	"percent": percent,
	"money":   money,
}

var xi18n *i18n.TranslatorFactory

func CustomFilters(env *stick.Env) {

	for name, method := range customFilters {
		env.Filters[name] = method
	}
}

func trans(ctx stick.Context, val stick.Value, args ...stick.Value) stick.Value {
	var useLang string

	values := ctx.Scope().All()

	if lang, found := values["lang"]; found {
		useLang = lang.(string)
	}

	lang, errors := Translator(useLang)

	if len(errors) > 0 {
		return fmt.Sprint(errors)
	}

	var assoc map[string]string

	if len(args) > 0 {
		if stick.IsMap(args[0]) {
			assoc = make(map[string]string)
			value := args[0].(map[string]stick.Value)
			for k, v := range value {
				assoc[k] = stick.CoerceString(v)
			}
		}
	}

	t, errs := lang.Translate(stick.CoerceString(val), assoc)

	if len(errs) > 0 {
		return fmt.Sprint(errs)
	}

	return t
}

func plural(ctx stick.Context, val stick.Value, args ...stick.Value) stick.Value {
	var useLang string

	if len(args) < 2 {
		return "plural <number> <string>"
	}
	values := ctx.Scope().All()

	if lang, found := values["lang"]; found {
		useLang = lang.(string)
	}

	lang, errors := Translator(useLang)

	if len(errors) > 0 {
		return fmt.Sprint(errors)
	}

	p, errs := lang.Pluralize(stick.CoerceString(val), stick.CoerceNumber(args[0]), stick.CoerceString(args[1]))

	if len(errs) > 0 {
		return fmt.Sprint(errs)
	}

	return p
}

func numeral(ctx stick.Context, val stick.Value, args ...stick.Value) stick.Value {
	var useLang string

	values := ctx.Scope().All()

	if lang, found := values["lang"]; found {
		useLang = lang.(string)
	}

	lang, errors := Translator(useLang)

	if len(errors) > 0 {
		return fmt.Sprint(errors)
	}

	return lang.FormatNumber(stick.CoerceNumber(val))
}

func percent(ctx stick.Context, val stick.Value, args ...stick.Value) stick.Value {
	var useLang string

	values := ctx.Scope().All()

	if lang, found := values["lang"]; found {
		useLang = lang.(string)
	}

	lang, errors := Translator(useLang)

	if len(errors) > 0 {
		return fmt.Sprint(errors)
	}

	return lang.FormatPercent(stick.CoerceNumber(val))
}

func money(ctx stick.Context, val stick.Value, args ...stick.Value) stick.Value {
	var useLang string

	values := ctx.Scope().All()

	if lang, found := values["lang"]; found {
		useLang = lang.(string)
	}

	lang, errors := Translator(useLang)

	if len(errors) > 0 {
		return fmt.Sprint(errors)
	}

	var symbol string

	if len(args) > 0 {
		symbol = stick.CoerceString(args[0])
	}

	m, err := lang.FormatCurrency(stick.CoerceNumber(val), symbol)

	if err != nil {
		return err.Error()
	}

	return m
}

func Translator(langCode string) (*i18n.Translator, []error) {
	if xi18n == nil {
		docDir := Config.Get("doc_dir").(string)
		rulesPath := docDir + "/lang/rules"
		messagesPath := docDir + "/lang/messages"

		var errs []error
		if xi18n, errs = i18n.NewTranslatorFactory(
			[]string{rulesPath},
			[]string{messagesPath},
			"en",
		); len(errs) > 0 {
			for _, err := range errs {
				logger.Warn(err)
			}
		}
	}

	return xi18n.GetTranslator(langCode)
}
