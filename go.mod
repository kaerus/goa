module bitbucket.org/kaerus/goa

go 1.13

require (
	github.com/BurntSushi/toml v0.3.0 // indirect
	github.com/fsnotify/fsnotify v1.4.7
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/jinzhu/configor v1.0.0
	github.com/jmoiron/sqlx v0.0.0-20180614180643-0dae4fefe7c0
	github.com/kardianos/osext v0.0.0-20170510131534-ae77be60afb1
	github.com/lib/pq v1.3.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/shopspring/decimal v0.0.0-20180301213236-69b3a8ad1f5f
	github.com/vube/i18n v0.0.0-20170329174511-75e795684593
	golang.org/x/sys v0.0.0-20180704094941-151529c776cd // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/yaml.v1 v1.0.0-20140924161607-9f9df34309c0 // indirect
	gopkg.in/yaml.v2 v2.2.1 // indirect
)
