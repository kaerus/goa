package goa

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"sync/atomic"
	"time"
)

type Logger interface {
	Debug(...interface{})
	Info(...interface{})
	Warn(...interface{})
	Error(...interface{})
	Print(...interface{})
	Println(...interface{})
	Printf(string, ...interface{})
	Fatal(...interface{})
	Fatalf(string, ...interface{})
	Flush()
}

var locker int32

type GoaLogger struct {
	lock   *int32
	Level  int
	Prefix string
	With   string
	Out    io.Writer
	buffer bytes.Buffer
}

func (logger *GoaLogger) New(prefix string, withs ...string) Logger {
	var with string

	for _, s := range withs {
		with += s + " "
	}

	if prefix == "" {
		prefix = logger.Prefix
	}

	log := GoaLogger{
		lock:   logger.lock,
		With:   with,
		Level:  logger.Level,
		Prefix: prefix,
		Out:    logger.Out,
		buffer: bytes.Buffer{},
	}

	return log
}

type LogLevel int

const (
	DebugLevel int = iota
	InfoLevel
	WarnLevel
	ErrorLevel
	QuiteLevel = 0xffffffff
)

const (
	TextLogFormat string = "%s-%s: %s%s\n"
)

func (log *GoaLogger) Log(severity string, message string) {

	// strip newline at end
	if message[len(message)-1] == '\n' {
		message = message[:len(message)-1]
	}
	var unlocked int32
	if atomic.CompareAndSwapInt32(log.lock, unlocked, int32(1)) {
		if log.buffer.Len() > 0 {
			log.buffer.WriteTo(log.Out)
		}
		fmt.Fprintf(log.Out, TextLogFormat, log.Prefix, severity, log.With, message)

		*log.lock = 0
	} else {
		fmt.Fprintf(&log.buffer, TextLogFormat, log.Prefix, severity, log.With, message)
	}
}

func (log GoaLogger) Format(severity string, msg ...interface{}) {
	m := fmt.Sprintf("%v", msg)
	messages := strings.Split(strings.Trim(m, "[ ]"), "\n")
	for _, message := range messages {
		if message != "" {
			log.Log(severity, message)
		}
	}
}

func (log GoaLogger) Print(m ...interface{}) {
	message := fmt.Sprint(m...)
	log.Log("LOG", message)
}

func (log GoaLogger) Println(m ...interface{}) {
	message := fmt.Sprint(m...)
	log.Log("LOG", message)
}

func (log GoaLogger) Printf(format string, m ...interface{}) {
	message := fmt.Sprintf(format, m...)
	log.Log("LOG", message)
}

func (log GoaLogger) Fatal(msg ...interface{}) {
	log.Format("FATAL", msg...)
	os.Exit(-1)
}
func (log GoaLogger) Fatalf(format string, m ...interface{}) {
	message := fmt.Sprintf(format, m...)
	log.Format(format, message)
	os.Exit(-1)
}

func (log GoaLogger) Debug(msg ...interface{}) {
	if log.Level < InfoLevel {
		log.Format("DEBUG", msg...)
	}
}
func (log GoaLogger) Info(msg ...interface{}) {
	if log.Level < WarnLevel {
		log.Format("INFO", msg...)
	}
}
func (log GoaLogger) Warn(msg ...interface{}) {
	if log.Level < ErrorLevel {
		log.Format("WARNING", msg...)
	}
}
func (log GoaLogger) Error(msg ...interface{}) {
	if log.Level < QuiteLevel {
		log.Format("ERROR", msg...)
	}
}
func (log GoaLogger) Flush() {
	var unlocked int32

	if log.buffer.Len() > 0 {
		for {
			if atomic.CompareAndSwapInt32(log.lock, unlocked, int32(1)) {
				if log.buffer.Len() > 0 {
					log.buffer.WriteTo(log.Out)
				}
				*log.lock = 0
				break
			}
		}
	}
}

func Log(req *http.Request) Logger {
	return req.Context().Value(LoggerKey).(Logger)
}

func ResponseLogger(w http.ResponseWriter, req *http.Request, next func(*http.Request)) {
	log := Log(req)
	timestamp := time.Now()
	next(req)

	// pick up status code from server.Response
	// TODO: cehck if go1.11 fixes so that we can
	// get the status code more directly
	statusCode := 0

	if response, ok := w.(*Response); ok {
		statusCode = response.Code
	}

	responseTime := time.Since(timestamp).String()

	log.Info(statusCode, responseTime)

}
