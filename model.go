package goa

import (
	"reflect"
)

var registry map[string]interface{}

func AddModel(models ...interface{}) {

	if registry == nil {
		registry = make(map[string]interface{})
	}

	for _, mod := range models {
		var m reflect.Type
		if t := reflect.TypeOf(mod); t.Kind() == reflect.Ptr {
			m = t.Elem()
		} else {
			m = t
		}

		registry[m.Name()] = mod
	}
}

func Models() map[string]interface{} {
	return registry
}
