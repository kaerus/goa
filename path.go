package goa

import (
	"regexp"
	"strings"
	"unicode/utf8"
)

// regexp for url parsing
const urlPattern = `^?([a-z]*:?/{2}?[^/]*)?` + // connection string
	`/?([^#^?]+)?` + // request path
	`(?:\?([^#]+))?` + // query string
	`(?:#([^?]+))?` // anchor string

// Matcher with a compiled regexp
type Matcher = *regexp.Regexp

var (
	urlExp Matcher
)

// URL string components
type URL struct {
	// Conn connection string
	Conn string
	// Path base file path
	Path string
	// Query string
	Query string
	// Anchor string
	Anchor string
}

// Path ...
type Path struct {
	Prefix string
	Parts  []Part
	Match  Matcher
}

// Part ...
type Part struct {
	Num    int
	Prefix string
	Name   string
	Match  string
}

// Param ...
type Param struct {
	Name  string
	Value interface{}
}

func init() {
	// precompile useful regexp
	urlExp = regexp.MustCompile(urlPattern)
}

// ParseURL returns URL string components
func ParseURL(url string) URL {
	u := urlExp.FindStringSubmatch(url)

	return URL{u[1], u[2], u[3], u[4]}
}

type stateFn func(*lexer) stateFn

type tokenType int

const (
	tErr tokenType = (iota - 1)
	tPart
	tParam
	tRegExp
	tOption
	tAsterix
)

type token struct {
	typ tokenType
	val string
}

type lexer struct {
	input  string
	start  int
	pos    int
	width  int
	tokens []token
	state  stateFn
}

func (l *lexer) emit(t tokenType) {
	l.tokens = append(l.tokens, token{t, l.input[l.start:l.pos]})
	l.start = l.pos
}
func (l *lexer) accept(valid string) bool {
	if strings.IndexRune(valid, l.next()) >= 0 {
		return true
	}
	l.backup()
	return false
}
func (l *lexer) next() (r rune) {
	if l.pos >= len(l.input) {
		l.width = 0
		return 0
	}
	r, l.width = utf8.DecodeRuneInString(l.input[l.pos:])
	l.pos += l.width
	return r
}
func (l *lexer) ignore() {
	l.start = l.pos
}
func (l *lexer) backup() {
	l.pos -= l.width
}
func (l *lexer) peek() (r rune) {
	r = l.next()
	l.backup()
	return r
}

func lexPath(l *lexer) stateFn {
	if l.pos >= len(l.input) {
		return nil
	}

	for {
		if hasPrefix(l.input[l.pos:], "/") {
			return lexPart
		}
		if l.next() == 0 {
			break
		}
	}
	if l.pos > l.start {
		l.emit(tPart)
	}

	return nil
}
func lexPart(l *lexer) stateFn {
	l.next()
	l.ignore()

	if hasPrefix(l.input[l.pos:], ":") {
		return lexParam
	} else if hasPrefix(l.input[l.pos:], "*") {
		return lexWild
	}

	for {
		if hasPrefix(l.input[l.pos:], "/") {
			l.emit(tPart)
			return lexPath
		}
		if l.next() == 0 {
			l.emit(tPart)
			break
		}
	}

	return lexPath
}

func lexParam(l *lexer) stateFn {
	l.next()
	l.ignore()
	for {
		if hasPrefix(l.input[l.pos:], "(") {
			l.emit(tParam)
			return lexRegexp
		} else if hasPrefix(l.input[l.pos:], "/") {
			l.emit(tParam)
			return lexPath
		}

		if l.next() == 0 {
			l.emit(tParam)
			break
		}
	}

	return nil
}

func lexRegexp(l *lexer) stateFn {

	for {
		c := l.next()
		p := l.peek()

		if c == ')' && (p == '/' || p == 0) {
			l.emit(tRegExp)
			return lexPath
		} else if p == 0 {
			break
		}
	}

	l.emit(tErr)
	return nil
}

func lexOpt(l *lexer) stateFn {
	l.next()
	l.ignore()

	if hasPrefix(l.input[l.pos:], "/") {
		l.emit(tOption)
		return lexPart
	}
	l.emit(tErr)
	return nil
}

func lexWild(l *lexer) stateFn {
	l.next()
	l.ignore()

	if hasPrefix(l.input[l.pos:], "/") {
		l.emit(tAsterix)
		return lexPart
	}
	l.emit(tErr)
	return nil
}

func lex(input string) *lexer {
	l := &lexer{
		input: input,
		state: lexPath,
	}

	return l
}

func (l *lexer) scan() {
	for state := lexPath; state != nil; {
		state = state(l)
	}
}

func Parse(path string) *Path {
	l := lex(path)
	l.scan()
	var parts []Part
	pos := 0

	for num, t := range l.tokens {
		switch t.typ {
		case tPart:
			prefix := "/" + t.val
			parts = append(parts, Part{Prefix: prefix, Num: num})
			pos++
		case tParam:
			match := ""
			switch t.val {
			case "id":
				match = "([\\d]+)"
			default:
				match = "([a-z0-9]+)"
			}
			parts = append(parts, Part{Prefix: "/", Name: t.val, Match: match, Num: num})
			pos++
		case tRegExp:
			parts[pos-1].Match = t.val
		}
	}

	prefix := "/"

	if len(l.tokens) > 0 {
		prefix = parts[0].Prefix
	}

	matchPattern := ""
	for _, part := range parts {
		matchPattern += part.Prefix + part.Match
	}
	var match Matcher

	if matchPattern != "" {
		match = regexp.MustCompile("^" + matchPattern + "$")
	}

	return &Path{Prefix: prefix, Parts: parts, Match: match}
}

func hasPrefix(s, prefix string) bool {
	return len(s) >= len(prefix) && s[:len(prefix)] == prefix
}

func hasSuffix(s, suffix string) bool {
	wildcard := suffix[:1]
	match := suffix[1:]
	switch wildcard {
	case "*":
		if len(match) == 0 {
			return true
		}
		return len(s) >= len(match) && s[len(s)-len(match):] == match
	case "$":
		return s == match
	}
	return false
}

func trimSplit(path string) []string {
	l := 0
	r := len(path)

	if r < 2 {
		return nil
	}

	if path[l] == '/' {
		l++
	}

	if path[r-1] == '/' {
		r--
	}

	parts := strings.Split(path[l:r], "/")

	return parts
}
