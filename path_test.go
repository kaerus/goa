package goa

import (
	"reflect"
	"testing"
)

func TestParseURL(t *testing.T) {
	type args struct {
		url string
	}
	tests := []struct {
		name string
		args args
		want URL
	}{
		{
			"empty",
			args{""},
			URL{"", "", "", ""},
		},
		{
			"slash",
			args{"/"},
			URL{"", "", "", ""},
		},
		{
			"http conn",
			args{"http://http.test.com"},
			URL{"http://http.test.com", "", "", ""},
		},
		{
			"https conn",
			args{"https://https.com"},
			URL{"https://https.com", "", "", ""},
		},
		{
			"ssh conn",
			args{"ssh://test.me.com"},
			URL{"ssh://test.me.com", "", "", ""},
		},
		{
			"ftp conn",
			args{"ftp://username:password@ftp.me.com"},
			URL{"ftp://username:password@ftp.me.com", "", "", ""},
		},
		{
			"conn path",
			args{"https://test.com/path"},
			URL{"https://test.com", "path", "", ""},
		},
		{
			"conn path file",
			args{"https://test.com/path/file.format"},
			URL{"https://test.com", "path/file.format", "", ""},
		},
		{
			"conn path query",
			args{"https://test.com/path/file.format?query=string"},
			URL{"https://test.com", "path/file.format", "query=string", ""},
		},
		{
			"conn path hash query",
			args{"https://test.com/path/file.format?query=string#!hashbang"},
			URL{"https://test.com", "path/file.format", "query=string", "!hashbang"},
		},
		{
			"path hash query",
			args{"/path/file.format?query=string#!hashbang"},
			URL{"", "path/file.format", "query=string", "!hashbang"},
		},
		{
			"query hash",
			args{"/?query=string#!hashbang"},
			URL{"", "", "query=string", "!hashbang"},
		},
		/* note: fails, but is it a valid test (see above)?
		{
			"hash query",
			args{"/#!hashbang?query=string"},
			URL{"", "", "query=string", "!hashbang"},
		},
		*/
		{
			"slash query",
			args{"/?query=string"},
			URL{"", "", "query=string", ""},
		},
		{
			"no slash query",
			args{"?query=string"},
			URL{"", "", "query=string", ""},
		},
		{
			"no slash query & query",
			args{"?query=string&another=string"},
			URL{"", "", "query=string&another=string", ""},
		},
		{
			"no slash hash path",
			args{"#/hash/path"},
			URL{"", "", "", "/hash/path"},
		},
		{
			"named path parameters 1",
			args{"/controller/:user"},
			URL{"", "controller/:user", "", ""},
		},
		{
			"named path parameters 2",
			args{"/controller/:user/account"},
			URL{"", "controller/:user/account", "", ""},
		},
		{
			"wildcarded path parameters 2",
			args{"/controller/*/:user.png"},
			URL{"", "controller/*/:user.png", "", ""},
		},
		{
			"relative conn path",
			args{"//cdn.com/path"},
			URL{"//cdn.com", "path", "", ""},
		},
		{
			"reverse proxy",
			args{"http://test.com@my.loadbalancers.net"},
			URL{"http://test.com@my.loadbalancers.net", "", "", ""},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseURL(tt.args.url); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseURL() = %v, want %v", got, tt.want)
			}
		})
	}
}
