package goa

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"sort"
	"strings"
)

type MiddlewareHandler func(w http.ResponseWriter, req *http.Request, next func(*http.Request))

type Router struct {
	routes     map[string][]Route
	lookup     []string
	forward    map[string]Forwarder
	middleware []MiddlewareHandler
}

type Route struct {
	path    *Path
	method  string
	handler Handler
}

type Forwarder struct {
	scheme string
	method string
	prefix string
	path   string
	proxy  *httputil.ReverseProxy
}

func NewRouter() *Router {
	router := &Router{}
	router.routes = make(map[string][]Route)
	router.forward = make(map[string]Forwarder)

	return router
}

// Reload routes
func (r *Router) Reload() {
	lookup := make([]string, 0, len(r.routes))

	for prefix := range r.routes {
		lookup = append(lookup, prefix)
	}

	sort.Sort(sort.Reverse(sort.StringSlice(lookup)))

	r.lookup = lookup
	logger.Info("reloaded routing tables")

}

func (r *Router) Route(w http.ResponseWriter, req *http.Request) {
	path := req.URL.Path
	host := req.Header.Get("host")

	if host == "" {
		host = "localhost"
	}
	if forward, ok := r.forward[host]; ok {
		if forward.method == "" || forward.method == req.Method {
			if forward.path == "" || hasPrefix(path, forward.path) {
				forward.proxy.ServeHTTP(w, req)
				return
			}
		}
	} else {
		for _, prefix := range r.lookup {
			if hasPrefix(path, prefix) {
				for _, route := range r.routes[prefix] {
					if route.method == req.Method || (route.method == "GET" && req.Method == "HEAD") {
						// check request path and params
						if params, err := requestParams(route.path, req); err == nil {
							request := setContextValue(req, ParameterKey, params)
							route.serve(w, request, &r.middleware)
							return
						}
					}
				}
			}
		}
	}

	http.Error(w, "404 not found", http.StatusNotFound)
}

func (route *Route) serve(w http.ResponseWriter, req *http.Request, mw *[]MiddlewareHandler) {
	mlen := len(*mw)

	if mlen > 0 {
		route.middleware(w, req, 0, mw)

	} else {
		route.handler(w, req)
	}
}

func (route *Route) middleware(w http.ResponseWriter, req *http.Request, mi int, mw *[]MiddlewareHandler) {

	m := (*mw)[mi]

	m(w, req, func(request *http.Request) {
		if request == nil {
			request = req
		}
		next := mi + 1
		if next < len(*mw) {
			route.middleware(w, request, next, mw)
		} else {
			route.handler(w, request)
		}
	})
}

func (r *Router) Use(mw MiddlewareHandler) {
	r.middleware = append(r.middleware, mw)
}

func (r *Router) Register(method, rawurl string, handler Handler) {
	u, _ := url.Parse(rawurl)

	if u.Opaque == "" {
		path := Parse(u.Path)
		route := Route{path, method, handler}
		r.routes[path.Prefix] = append(r.routes[path.Prefix], route)
		logger.Info(method, u.Path)
	} else {
		// rawurl is "<scheme>:<source>@<destination>"
		p := strings.Split(u.Opaque, "@")
		if len(p) < 2 {
			logger.Info("invalid proxy path:", rawurl)
			return
		}

		src := proxyURL(p[0])
		dest := proxyURL(p[1])

		forward := Forwarder{
			scheme: u.Scheme,
			method: method,
			path:   src.Path,
			proxy: &httputil.ReverseProxy{
				Director: func(req *http.Request) {
					req.URL.Scheme = u.Scheme
					req.URL.Host = dest.Host
					if dest.Path != "" && dest.Path != "/" {
						req.URL.RawPath = dest.Path + req.URL.RawPath
					}
					req.Host = dest.Host
				},
			},
		}
		logger.Info(method, src.Host+src.Path, "->", u.Scheme, dest.Host+dest.Path)
		r.forward[src.Host] = forward
	}
}

func GetParams(req *http.Request) Params {
	return req.Context().Value(ParameterKey).(Params)
}

func proxyURL(rawurl string) *url.URL {
	u, _ := url.Parse(rawurl)

	if u.Host == "" {
		s := strings.SplitN(u.Path, "/", 1)
		if len(s) == 2 {
			u.Host = s[0]
			u.Path = s[1]
		} else {
			u.Host = u.Path
			u.Path = ""
		}
	}

	return u
}
func requestParams(path *Path, req *http.Request) (map[string]interface{}, error) {
	params := make(map[string]interface{})

	if path.Match == nil {
		return params, nil
	}

	parts := trimSplit(req.URL.Path)

	matched := path.Match.FindStringSubmatch(req.URL.Path)

	if len(matched) == 0 {
		return nil, fmt.Errorf("no route match")
	}

	m := 1
	for p, part := range path.Parts {
		if part.Name != "" {
			if part.Match != "" {
				params[part.Name] = matched[m]
				m++
			} else {
				params[part.Name] = parts[p]
			}
		}
	}

	return params, nil
}

// Set context...
func setContextValue(req *http.Request, key interface{}, val interface{}) *http.Request {
	if val == nil {
		return req
	}

	return req.WithContext(context.WithValue(req.Context(), key, val))
}

// Get context...
func getContextValue(req *http.Request, key interface{}) interface{} {
	return req.Context().Value(key)
}
