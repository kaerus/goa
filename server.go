package goa

import (
	"context"
	"database/sql"
	"net/http"
	"os"
	"path/filepath"

	"github.com/jinzhu/configor"
)

type Params = map[string]interface{}

var logger GoaLogger

type ContextKey int

const (
	DatabaseKey ContextKey = iota
	ParameterKey
	SessionKey
	LoggerKey
)

type Configuration interface {
	Get(key string) interface{}
	Set(key string, value interface{})
}

var Config Configuration

type Server struct {
	Router *Router
	DB     *sql.DB
}

type Response struct {
	http.ResponseWriter
	Code int
}

func (r *Response) Header() http.Header {
	return r.ResponseWriter.Header()
}

func (r *Response) WriteHeader(code int) {
	if r.Code == -1 {
		r.Code = code
		r.ResponseWriter.WriteHeader(code)
	}
}

func (r *Response) Write(b []byte) (int, error) {
	if r.Code == -1 {
		r.Code = http.StatusOK
	}

	return r.ResponseWriter.Write(b)
}

// Register request handler
func (s *Server) Register(method, selector string, handler Handler) *Server {

	s.Router.Register(method, selector, handler)

	return s
}

// SetveHTTP handler
func (s *Server) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	res := &Response{w, -1}
	uri := req.RequestURI
	if uri == "" {
		uri = req.URL.RequestURI()
	}
	clientIP := req.RemoteAddr
	if ip, ok := req.Header["X-Forwarded-For"]; ok {
		clientIP = ip[0]
	}
	log := logger.New(clientIP, req.Method, uri)
	defer log.Flush()
	request := req.WithContext(context.WithValue(req.Context(), LoggerKey, log))
	s.Router.Route(res, request)
}

// New server
func New(config Configuration, env string) *Server {
	var (
		app  string
		root string
		err  error
	)

	logger = GoaLogger{Prefix: "server", Out: os.Stdout, lock: &locker}

	if env == "prod" {
		logger.Level = InfoLevel
	} else if env == "dev" {
		logger.Level = DebugLevel
	} else if env == "test" {
		logger.Level = ErrorLevel
	}

	Config = config

	Config.Set("logger", logger)
	// sets app root directory
	if app, err = os.Executable(); err != nil {
		panic(err)
	}
	root = filepath.Dir(app)

	// create server instance
	s := &Server{Router: NewRouter()}

	// env config is config.<env>.yml with base config from config.yml
	configFile := root + "/config/config.yml"
	if err = configor.New(&configor.Config{Environment: env}).Load(config, configFile); err != nil {
		panic(err)
	}

	// overrides env setting from config when a user defined env is set
	if env != "" {
		Config.Set("env", env)
	}

	Config.Set("app_dir", root)

	// set current working directory to DocRoot so that
	// we can use relative paths inside view templates
	docDir := root + "/web"
	if err := os.Chdir(docDir); err != nil {
		panic(err)
	}

	Config.Set("doc_dir", docDir)

	return s
}

// With controllers
func (s *Server) With(controllers ...func(*Controller)) *Server {

	for _, co := range controllers {
		c := AddController(co)
		for _, method := range c.Methods() {
			s.Router.Register(method.Type, method.Path, method.Handle)
		}
	}

	return s
}

// Use middleware
func (s *Server) Use(middlewares ...MiddlewareHandler) *Server {

	for _, mw := range middlewares {
		s.Router.Use(mw)
	}

	return s
}

// Start server
func (s *Server) Start(listener string) {
	s.Router.Reload()

	panic(http.ListenAndServe(listener, s))
}
