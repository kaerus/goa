package goa

import (
	"context"
	"net/http"

	"bitbucket.org/kaerus/goa/session"
)

func Session(sessions interface{}) func(http.ResponseWriter, *http.Request, func(*http.Request)) {
	return func(w http.ResponseWriter, req *http.Request, next func(*http.Request)) {
		request := req.WithContext(context.WithValue(req.Context(), SessionKey, sessions))
		next(request)
	}
}

func Sessions(req *http.Request) *session.Manager {
	return req.Context().Value(SessionKey).(*session.Manager)
}
