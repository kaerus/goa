package types

import (
	"bytes"
	"database/sql/driver"
	"encoding/json"
	"fmt"
)

type JSON map[string]interface{}
type JSONA []map[string]interface{}

func EncodeToJSON(t interface{}) ([]byte, error) {
	buffer := &bytes.Buffer{}
	encoder := json.NewEncoder(buffer)
	encoder.SetEscapeHTML(false)
	err := encoder.Encode(t)
	if err != nil {
		return nil, err
	}
	return buffer.Bytes(), err
}

func (j JSON) Value() (driver.Value, error) {
	jsn, err := json.Marshal(&j)

	if err != nil {
		return "", err
	}

	return string(jsn[:]), err
}

func (j JSON) MarshalJSON() (*[]byte, error) {
	val, err := EncodeToJSON(&j)

	if err != nil {
		return nil, err
	}

	if len(val) == 0 {
		return nil, nil
	}

	return &val, err
}

func (j JSON) Json() []byte {
	val, _ := EncodeToJSON(&j)

	return val
}
func (j *JSON) Scan(value interface{}) (err error) {
	if v, ok := value.(string); ok {
		err = json.Unmarshal([]byte(v), &j)
	} else if v, ok := value.([]uint8); ok {
		err = json.Unmarshal([]byte(v), &j)
	} else {
		err = fmt.Errorf("unable to scan from %T", value)
	}

	return err
}

func (j JSONA) Value() (driver.Value, error) {
	jsn, err := j.MarshalJSON()

	if err != nil {
		return "", err
	}
	if jsn == nil {
		return "", nil
	}
	return string((*jsn)[:]), err
}

func (j *JSONA) Scan(value interface{}) error {
	return json.Unmarshal(value.([]byte), &j)
}

func (j JSONA) MarshalJSON() (*[]byte, error) {
	val, err := EncodeToJSON(j)

	if err != nil {
		return nil, err
	}

	if len(val) == 0 {
		return nil, nil
	}

	return &val, err
}
