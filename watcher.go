package goa

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"syscall"

	"os"
	"os/exec"
	"path/filepath"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/kardianos/osext"
)

type Watch struct {
	root     string
	fileList map[string]bool
	watcher  *fsnotify.Watcher
	reqChn   chan fsnotify.Event
	repChn   chan error
	kinds    []string
	paths    []string
}

func (s *Server) Watcher(kinds ...string) (*Watch, error) {
	w := &Watch{}

	watcher, err := fsnotify.NewWatcher()

	if err != nil {
		return nil, err
	}

	w.watcher = watcher

	w.root = Config.Get("app_dir").(string)
	w.kinds = kinds
	w.fileList = map[string]bool{}

	w.reqChn = make(chan fsnotify.Event)
	w.repChn = make(chan error)

	return w, nil
}

func (w *Watch) Watch(paths ...string) error {

	go w.watchHandler()
	go w.buildHandler()

	for _, p := range paths {
		prefix := p[:1]
		if prefix == "+" || prefix == "-" {
			fp := w.root + p[1:]
			if fpaths, err := filepath.Glob(fp); err == nil {
				watch := prefix == "+"
				for _, f := range fpaths {
					w.Add(f, watch)
				}
			} else {
				return err
			}

		} else {
			p = w.root + p
			w.Add(p, true)
		}
	}

	watching := 0
	for k, v := range w.fileList {
		if v && w.isValid(k) {
			watching++
			if err := w.watcher.Add(k); err != nil {
				return fmt.Errorf(k + " " + err.Error())
			}
		}
	}

	logger.Info("watching", watching, "files")
	return nil
}

func (w *Watch) isValid(f string) bool {
	if len(w.kinds) == 0 {
		return true
	}

	for _, k := range w.kinds {
		kl := len(k)
		fl := len(f)
		fk := f[fl-kl:]
		if k == fk {
			return true
		}
	}

	return false
}
func (w *Watch) watchHandler() {
	inAction := false

	defer close(w.reqChn)
	defer close(w.repChn)

	for {
		select {
		case event := <-w.watcher.Events:
			if event.Op&fsnotify.Write == fsnotify.Write {
				if !inAction {
					inAction = true
					w.reqChn <- event
				}
			}
		case err := <-w.watcher.Errors:
			if err != nil {
				logger.Error("error:", err)
			}
		case err := <-w.repChn:
			if err != nil {
				logger.Error(err)
			}
			inAction = false
		}
	}
}
func (w *Watch) buildHandler() {
	binary, err := osext.Executable()
	if err != nil {
		panic(err)
	}
	makeCMD := "/usr/bin/make -C " + w.root
	for {
		<-w.reqChn
		time.Sleep(time.Millisecond * 500)
		cmd := exec.Command("sh", "-c", makeCMD)
		var out bytes.Buffer
		var stderr bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &stderr
		err := cmd.Run()
		if err != nil {
			w.repChn <- fmt.Errorf(stderr.String())
		} else {
			logger.Info(out.String())
			os.Chdir(w.root)

			err := syscall.Exec(binary, os.Args, os.Environ())
			if err != nil {
				w.repChn <- err
			} else {
				// perhaps should make a clean exist
				os.Exit(0)
			}
		}
	}
}

func (w *Watch) Add(f string, watch bool) {

	fInfo, fErr := os.Stat(f)

	if fErr != nil {
		logger.Error("stat: ", fErr)
	}

	if fInfo.IsDir() {
		files, err := ioutil.ReadDir(f)

		if err != nil {
			logger.Error("readDir: ", err)
		}

		for _, file := range files {
			w.Add(f+"/"+file.Name(), watch)
		}
	} else {
		w.fileList[f] = watch
	}
}
